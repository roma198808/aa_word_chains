class Node
  attr_accessor :parent, :children, :value

  def initialize(value)
    @parent = nil
    @children = []
    @value = value
  end
  
  def add_child(i_child)
    i_child.parent = self
    @children << i_child
  end
  
  def to_s
    puts
    puts "ID: #{self.object_id}"
    puts "Parent: #{self.parent.value}"
    puts "Children: #{self.children.map { |child| child.value }}"
    puts "Value: #{self.value}"
    puts
  end
end 